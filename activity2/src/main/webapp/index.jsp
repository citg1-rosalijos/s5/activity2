<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="ISO-8859-1">
		<title>Servlet Job Finder</title>
		<style>
			div {
				margin-top: 5px;
				margin-bottom: 5px;
			}
		</style>
	</head>
	<body>
		<h1>Welcome to Servlet Job Finder!</h1>
		<form action="register" method="post">
			<div>
				<label for="firstname">First Name:</label>
				<input name="firstname" type="text" required>
			</div>
			
			<div>
				<label for="lastname">Last Name:</label>
				<input name="lastname" type="text" required>
			</div>
			
			<div>
				<label for="phone">Phone Number:</label>
				<input name="phone" type="text" required>
			</div>
			
			<div>
				<label for="email">Email Address:</label>
				<input name="email" type="email" required> 
			</div>
			
			<fieldset>
				<legend>How did you discover the app?</legend>
				
				<input type="radio" id="friends" name="app_discovery" value="friends" required>
				<label for="friends">Friends</label><br>
					
				<input type="radio" id="social_media" name="app_discovery" value="social_media" required>
				<label for="social_media">Social Media</label><br>

				<input type="radio" id="others" name="app_discovery" value="others" required>
				<label for="others">Others</label><br>
			</fieldset>
			
			<div>
				<label for="birthdate">Date of Birth:</label>
				<input name="birthdate" type="date" required> 
			</div>
			
			<div>
				<label for="user_type">Are you an employer or an applicant?</label>
				<select id="user_type" name="user_type" required>
					<option value="" selected>Select one</option>
					<option value="applicant">Applicant</option>
					<option value="employer">Employer</option>
				</select>
			</div>
			
			<div>
				<label for="description">Profile Description:</label>
				<textarea name="description" maxlength="500" required></textarea>
			</div>
			
			<br>
			<input type="submit" value="Register">
		</form>
	</body>
</html>