<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="ISO-8859-1">
		<title>Home</title>
	</head>
	<body>
		<h1>Welcome <%= session.getAttribute("fullname") %>!</h1>
		<%
			String userType = session.getAttribute("userType").toString();
			
			if(userType.equals("applicant")) {
				out.println("<p>Welcome applicant. You may now start looking for your career opportunity.");
			}else {
				out.println("<p>Welcome employer. You may now start browsing applicant portfolio.");
			}
		%>
	</body>
</html>