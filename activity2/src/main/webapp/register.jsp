<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="ISO-8859-1">
		<title>Register Confirmation</title>
	</head>
	<body>
		<%
			String appDiscovery = session.getAttribute("appDiscovery").toString();
			String userType = session.getAttribute("userType").toString();
			
			switch(appDiscovery) {
				case "friends": appDiscovery = "Friends"; break;
				case "social_media": appDiscovery = "Social Media"; break;
				case "others": appDiscovery = "Others"; break;
			}
			
			switch(userType) {
				case "applicant": userType = "Applicant"; break;
				case "employer": userType = "Employeer"; break;
			}
		%>

		<h1>Register Confirmation</h1>
		
		<p>First Name: <%= session.getAttribute("firstname") %> </p>
		<p>Last Name: <%= session.getAttribute("lastname") %> </p>
		<p>Phone Number: <%= session.getAttribute("phone") %> </p>
		<p>Email: <%= session.getAttribute("email") %> </p>
		<p>App Discovery: <%= appDiscovery %> </p>
		<p>Date of Birth: <%= session.getAttribute("birthdate") %> </p>
		<p>User Type: <%= userType %> </p>
		<p>Description: <%= session.getAttribute("description") %> </p>
		
		<form action="login" method="post">
			<input type="submit">
		</form>
		<form action="index.jsp" method="post">
			<input type="submit" value="Back">
		</form>
	</body>
</html>