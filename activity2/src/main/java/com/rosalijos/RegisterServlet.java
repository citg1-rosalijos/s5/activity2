package com.rosalijos;

import java.io.IOException;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

@WebServlet("/register")
public class RegisterServlet extends HttpServlet {

	private static final long serialVersionUID = 3738154183780775129L;

	public void init() throws ServletException {
		System.out.println("RegisterServlet has been initialized.");
	}
	
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String firstname = request.getParameter("firstname");
		String lastname = request.getParameter("lastname");
		String phone = request.getParameter("phone");
		String email = request.getParameter("email");
		String appDiscovery = request.getParameter("app_discovery");
		String birthdate = request.getParameter("birthdate");
		String userType = request.getParameter("user_type");
		String description = request.getParameter("description");
		
		HttpSession session = request.getSession();
		
		session.setAttribute("firstname", firstname);
		session.setAttribute("lastname", lastname);
		session.setAttribute("phone", phone);
		session.setAttribute("email", email);
		session.setAttribute("appDiscovery", appDiscovery);
		session.setAttribute("birthdate", birthdate);
		session.setAttribute("userType", userType);
		session.setAttribute("description", description);
		
		response.sendRedirect("register.jsp");
	}
	
	public void destroy() {
		System.out.println("RegisterServlet has been finalized.");
	}
}
