package com.rosalijos;

import java.io.IOException;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {

	private static final long serialVersionUID = 1406473340012432654L;

	public void init() throws ServletException {
		System.out.println("RegisterServlet has been initialized.");
	}
	
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
		HttpSession session = request.getSession();
		String firstname = session.getAttribute("firstname").toString();
		String lastname = session.getAttribute("lastname").toString();
		session.setAttribute("fullname", firstname + " " + lastname);

		response.sendRedirect("home.jsp");
	}
	
	public void destroy() {
		System.out.println("RegisterServlet has been finalized.");
	}
}
